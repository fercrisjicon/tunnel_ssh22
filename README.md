# Tunnel SSH

## Descripcion

#### Modelo 1: Tunel directo (abierto a internet)
<div style="padding: 5%"><img src="Modelo01.png"/>
Descripcion
</div>

#### Modelo 2: Tuner directo (cerrado a internet)
<div style="padding: 5%"><img src="Modelo02.png"/>
Descripcion
</div>

#### Modelo 3: Tunel directo con rebote
<div style="padding: 5%"><img src="Modelo03.png"/>
Descripcion
</div>

#### Modelo 4: Tuner reverse
<div style="padding: 5%"><img src="Modelo04.png"/>
Descripcion
</div>

#### Modelo 5: Tuner reverse con rebote
<div style="padding: 5%"><img src="Modelo05.png"/>
Descripcion
</div>

###### Para futura practica
Desde otro ordenador(**remoto**):
1. instalar xinetd
> sudo apt install xinetd

2. abrir servidores (chargen, daytime, echo) con sus puertos (19, 7, 13)
> vim /etc/xinetd.d/chargen
```bash
# This is the tcp version.
service chargen
{
	disable		= no
	type		= INTERNAL
	id		= chargen-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}
```
> vim /etc/xinetd.d/daytime
```bash
# This is the tcp version.
service daytime
{
	disable		= no
	type		= INTERNAL
	id		= daytime-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}
```
> vim /etc/xinetd.d/echo
```bash
# This is the tcp version.
service echo
{
	disable		= no
	type		= INTERNAL
	id		= echo-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}
```
   - instalamos apache2
   > sudo apt install apache2
   - comprobar si funciona el apache2 desde:
     - navegador
        > localhost/index.html
     - terminal
        > telnet localhost 80
        > GET / HTTP/1.0
        ***Dos ENTERs***

3. creamos un nuevo server: daytime2(2013)
> vim /etc/xinetd.d/daytime2
```bash
service daytime2
{
  disable = no
  type	= UNLISTED
  socket_type = stream
  protocol = tcp
  wait = no
  redirect = 0.0.0.0
  bind = 127.0.0.1 13
  port = 2013
  user = nobody
```
# Practicas
## Practica 0: Tunnels SSH
### **Tunel directo**
Desde nuestro ordenador(**local**):
1. hacer un tunel directo, con nuestro puerto **50001**, al server daytime2(***2013***)
> ssh -L **50001**:localhost:***2013*** root@i15
    > -L = bind_address

2. en otra terminal, encender el server daytime con nuestro puerto 50001
> telnet localhost **50001**
```
Trying ::1...
Connected to localhost.
Escape character is '^]'.
22 MAR 2022 10:40:45 CET
Connection closed by foreign host.
```